/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {

    config.filebrowserBrowseUrl = '/filemanager/index.html?type=files';
    config.filebrowserImageBrowseUrl = '/filemanager/index.html?type=images';
    config.filebrowserFlashBrowseUrl = '/filemanager/index.html?type=flash';
    config.filebrowserUploadUrl = './filemanager/index.html?type=files';
    config.filebrowserImageUploadUrl = '/filemanager/index.html?type=images';
    config.filebrowserFlashUploadUrl = './filemanager/index.html?type=flash';
    config.removePlugins = 'resize';
    config.extraPlugins = 'sharedspace';


    // Define changes to default configuration here. For example:
    //config.language = 'fr';
    // config.uiColor = '#AADC6E';
    //FONTS
    //the next line add the new font to the combobox in CKEditor
    config.font_names = 'Lato/Lato;' + config.font_names;
    config.extraPlugins = 'timestamp';


    // Define changes to default configuration here. For example:
    //config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.extraPlugins = 'lineheight';
    config.fontSize_sizes = '0px;2px;4px;8px;10px;11px;12px;14px;16px;18px;20px;22px;24px;26px;28px;30px;32px;34px;36px;38px;40px;42px;44px;46px;46px;46px;';


    //config.language = 'gb';

    config.line_height = "0px;2px;4px;6px;8px;10px;12px;14px;16px;18px;20px;22px;24px;26px;28px;30px;32px;34px;36px;38px;40px;42px;44px;46px;48px;50px;52px;54px;56px;58px;60px";

    //config.language = 'gb';

    //  config.line_height="0px;2px;4px;6px;8px;10px;12px;14px;16px;18px;20px;22px;24px;26px;28px;30px;32px;34px;36px;38px;40px" ;
     config.toolbar_basic = [
        {name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
        {name: 'styles', items: ['Font', 'FontSize']},
        {name: 'others', items: ['lineheight']},
        {name: 'basicstyles', items: ['TextColor', '-', 'Bold', 'Italic', 'Underline', 'BulletedList']}]

    config.toolbar_verybasic = [
        {name: 'styles', items: ['Font', 'FontSize']},
        {name: 'basicstyles', items: ['TextColor', '-', 'Bold', 'Italic', 'Underline', 'BulletedList']}]


};
