
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


//import models
require('./models/Field.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('wizard', require('./components/Wizard.vue'));
Vue.component('ckeditor', require('./components/Ckeditor.vue'));
Vue.component('auth', require('./components/Auth.vue'));
Vue.component('tree', require('./components/tree.vue'));
Vue.component('noty', require('./components/Noty.vue'));

/*VIEW*/
Vue.component('view_admin_index', require('./views/admin/index.vue'));

/*ADMIN USERS*/
Vue.component('view-admin-users', require('./views/admin/users/index.vue'));
Vue.component('view-admin-users-edit', require('./views/admin/users/edit.vue'));

/*SIZES USERS*/
Vue.component('view-admin-sizes', require('./views/admin/sizes/index.vue'));
Vue.component('view-admin-sizes-edit', require('./views/admin/sizes/edit.vue'));
Vue.component('view-admin-sizes-templates', require('./views/admin/sizes/templates.vue'));

/*ADMIN CATEGORIES*/
Vue.component('view-admin-categories', require('./views/admin/categories/index.vue'));
Vue.component('view-admin-categories-edit', require('./views/admin/categories/edit.vue'));

/*ADMIN LOCATIONS*/
Vue.component('view-admin-locations', require('./views/admin/locations/index.vue'));
Vue.component('view-admin-locations-edit', require('./views/admin/locations/edit.vue'));

/*ADMIN LANGUAGES*/
Vue.component('view-admin-languages', require('./views/admin/languages/index.vue'));
Vue.component('view-admin-languages-edit', require('./views/admin/languages/edit.vue'));

/*ADMIN WORDS*/
Vue.component('view-admin-words', require('./views/admin/words/index.vue'));
Vue.component('view-admin-words-edit', require('./views/admin/words/edit.vue'));

/*ADMIN SETTINGS*/
Vue.component('view-admin-settings', require('./views/admin/settings/index.vue'));


const app = new Vue({
    el: 'body',
    data: {
    	text:'zero one',
    	html:'<h3>Hello world</h3>',
    	currentView :'view_admin_index',
    	menu:[
    		{ id:'dashboard', name:'Dashboard', ico:'fa-tachometer' , view:'admin/show'},
            { id:'users', name:'Users', ico:'fa-users' , view:'users/show'},
            { id:'sizes', name:'Sizes', ico:'fa-expand' , view:'sizes/show'},
    		{ id:'categories', name:'Categories', ico:'fa-list', view:'categories/show'},
    		{ id:'locations', name:'Locations', ico:'fa-map-marker', view:'locations/show'},
    		{ id:'languages', name:'Languages', ico:'fa-globe',  view:'languages/show'},
    		{ id:'dictionary', name:'Dictionary', ico:'fa-language', view:'words/show'},
            { id:'settings', name:'Settings', ico:'fa-cogs', view:'settings/show'}
    	],
        notifications:[]
    },
    methods: {

    	select: function(view){
            this.currentView = view;
    		this.$http.get(view).then(function(request){
    			
					$(this.$els.view).html(request.body);
					this.$compile(this.$els.view);
    		}, function(result){
                
    		});
    	},
        setUpNotifications: function(){

            var local = this;
           local.notifications.forEach(function(noty, index){
           
               if(noty.timeout != null)
               {
                   setTimeout(() => {
                     local.notifications.$remove(noty);
                    }, noty.timeout);
                }
            })
        }
        
    
    },
    events: {

        getView: function(view){

          this.$http.get(view).then(function(request){
                
                    $(this.$els.view).html(request.body);
                    this.$compile(this.$els.view);
            }, function(result){

           });
      },
      noty: function(noty){
        console.log(noty);
         this.notifications.push(noty);
         this.setUpNotifications();
      },
        execute: function(fn, noty){

          var fn_name = fn.substring(0, fn.indexOf('('));
          var params = fn.substring(fn.indexOf('(') + 1, fn.indexOf(')'));

          this.$broadcast(fn_name, params);
          if(noty != null)
            this.notifications.$remove(noty);

        }
    },
    ready: function(){

    	this.select(this.menu[2].view);
    }
});

