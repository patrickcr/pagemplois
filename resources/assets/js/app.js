/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


//import models
require('./models/Field.js');

var moment = require('moment');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('wizard', require('./components/Wizard.vue'));
Vue.component('ckeditor', require('./components/Ckeditor.vue'));
Vue.component('auth', require('./components/Auth.vue'));
Vue.component('tree', require('./components/tree.vue'));
Vue.component('noty', require('./components/Noty.vue'));

Vue.component('search-index', require('./views/search/index.vue'));

/*VIEWS*/
Vue.component('view-customers-index', require('./views/customers/index.vue'));


const app = new Vue({
    el: 'body',
    data:{
        notifications:[]
    },
    methods: {

        setUpNotifications: function () {

            var local = this;
            local.notifications.forEach(function (noty, index) {

                if (noty.timeout != null) {
                    setTimeout(() => {
                        local.notifications.$remove(noty);
                    }, noty.timeout);
                }
            })
        }
    },
    events: {

        noty: function (noty) {
            console.log(noty);
            this.notifications.push(noty);
            this.setUpNotifications();
        },
    }

});


Vue.filter('text', function(value) {
    return $(value).text();
});
Vue.filter('moment', function (date) {
    var d = moment(date);
    if (!d.isValid(date)) return null;
    return d.format.apply(d, [].slice.call(arguments, 1));
});

