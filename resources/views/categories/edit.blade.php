<view-admin-categories-edit inline-template :category="{{ $category->toJson() }}">


	<form @submit="save" method="POST" role="form">

		<legend>
			<span v-if="category.id > 0">EDIT CATEGORY</span>
			<span v-if="category.id == -1">NEW CATEGORY</span>
			<strong>@{{ category.name }}</strong></legend>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
				<label for="">Name</label>
				<input type="text" class="form-control" v-model="category.name" placeholder="ex:. Building and Construction">
				<span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
			</div>
			<div class="form-group col-md-6 h-100p" >
				<label for="">Parent</label>
				<select type="text" v-el:parent  class="form-control" v-model="category.parent">
					<option value="">None</option>
					@foreach($categories as $categoryDB)
					@if($categoryDB->id == $category->parent)
					<option selected value="{{ $categoryDB->id }}">{{ $categoryDB->name }}</option>
					@else
					<option value="{{ $categoryDB->id }}">{{ $categoryDB->name }}</option>	
					@endif
					@endforeach		
				</select>
			</div>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.reference.class">
				<label for="">Reference</label>
				<input type="text" class="form-control" v-model="category.reference" placeholder="ex:. building-and-construction">
				<span class="help-block" v-show="!fields.reference.state" v-text="fields.reference.msg"></span>
			</div>
			<div class="form-group col-md-4 h-100p active-options">
				<label for="" class="w100p">Active</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input   @change="checkActive(1)"  type="checkbox" v-model="active.on"  >
					<label for="checkbox1">YES

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input type="checkbox"  @change="checkActive(0)" v-model="active.off">
					<label for="checkbox1">NO 

					</label>
				</div>
			</div>

			<div class="form-group col-md-2 h-100p" v-bind:class="fields.reference.class">
				<label for="">Order</label>
				<input type="text" class="form-control" v-model="category.order" placeholder="ex:. 1">
			</div>

			<div class="form-controls tar">
				<button type="button" @click="dashboard()" class="btn btn-danger">CANCEL</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
			</div>
		</form>


	</view-admin-categories-edit>
