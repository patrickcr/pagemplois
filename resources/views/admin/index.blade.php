<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pagemplois</title>
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="css/app.css">
    {{-- PLugins --}}
    <link rel="stylesheet" type="text/css" href="plugins/antiscroll-master/antiscroll.css">
</head>
<script>
    window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
    ]); ?>
</script>
<body>
<div class="container no-padding">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--   <a class="navbar-brand" href="#">Brand</a> --}}
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav top-menu">

                    <li v-bind:class="currentView == iten.view ? 'active' : ''" v-for="iten in menu" @click="
                    select(iten.view)"><a>
                        <i class="fa" v-bind:class="iten.ico" aria-hidden="true"></i>
                        @{{ iten.name }}</a></li>

                </ul>
                {{--    <form class="navbar-form navbar-left" role="search">
                     <div class="form-group">
                       <input type="text" class="form-control" placeholder="Search">
                     </div>
                     <button type="submit" class="btn btn-default">Submit</button>
                   </form> --}}
                @include('auth.user_control')
            </div>
        </div>
    </nav>
    <div class="container master-content admin" style="width:100%;">
        <div v-el:view>
            @{{{ html }}}
        </div>


    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 noty no-padding bottom-right">
        <noty v-for="noty in notifications" :noty="noty"></noty>
    </div>
    <footer class="no-padding">
        <div>
            <ul>
                <li>TE</li>
            </ul>
        </div>
    </footer>
</div>

</body>
{{-- Plugins --}}
<script type="text/javascript">

    var z = null;
</script>

<script type="text/javascript" src="../../plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="js/admin.js"></script>

</html>