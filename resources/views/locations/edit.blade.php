<view-admin-locations-edit inline-template :location="{{ $location->toJson() }}">

	<form @submit="save"  role="form">

		<legend>
			<span v-if="location.id > 0">EDIT LOCATION</span>
			<span v-if="location.id == -1">NEW LOCATION</span>
			<strong>@{{ location.name }}</strong></legend>
			<div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
				<label for="">Name</label>
				<input type="text" class="form-control" v-model="location.name" placeholder="ex:. Lausanne">
				<span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
			</div>
			<div class="form-group col-md-6 h-100p" >
				<label for="">Parent</label>
				<select type="text" v-el:parent  class="form-control" v-model="location.parent">
					<option value="">None</option>
					@foreach($locations as $locationDB)
					@if($locationDB->id == $location->parent)
					<option selected value="{{ $location->id }}">{{ $locationDB->name }}</option>
					@else
					<option value="{{ $locationDB->id }}">{{ $locationDB->name }}</option>	
					@endif
					@endforeach		
				</select>
			</div>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.reference.class">
				<label for="">Reference</label>
				<input type="text" class="form-control" v-model="location.reference" placeholder="ex:. lausanne">
				<span class="help-block" v-show="!fields.reference.state" v-text="fields.reference.msg"></span>
			</div>
			<div class="form-group col-md-4 h-100p active-options">
				<label for="" class="w100p">Active</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input   @change="checkActive(1)"  type="checkbox" v-model="active.on"  >
					<label for="checkbox1">YES

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input type="checkbox"  @change="checkActive(0)" v-model="active.off">
					<label for="checkbox1">NO 

					</label>
				</div>
			</div>
			<div class="form-group col-md-2 h-100p" v-bind:class="fields.reference.class">
				<label for="">Order</label>
				<input type="text" class="form-control" v-model="location.order" placeholder="ex:0">
			</div>
			<div class="form-controls tar">
				<button type="button" @click="dashboard()" class="btn btn-danger">CANCEL</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
			</div>
		</form>


	</view-admin-locations-edit>
