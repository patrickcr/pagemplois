@extends('master')

@section('content')


    @if($settings->status)

            @{{ data }}

    @else
        <div class="col-md-offset-4 col-md-4"></div>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Settings</h3>
            </div>
            <div class="panel-body">
                <h5>
                    The system settings are not set, please login with you administrator account and fill the settings
                    section.
                </h5>
                <a class="btn btn-primary" href="/login">LOGIN</a>
            </div>
        </div>
    @endif
@endsection