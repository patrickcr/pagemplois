<view-admin-languages-edit inline-template :language="{{ $language->toJson() }}">


	<form @submit="save" method="POST" role="form">

		<legend>
			<span v-if="language.id > 0">EDIT CATEGORY</span>
			<span v-if="language.id == -1">NEW CATEGORY</span>
			<strong>@{{ language.name }}</strong></legend>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
				<label for="">Name</label>
				<input type="text" class="form-control" v-model="language.name" placeholder="ex:. FRENCH">
				<span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
			</div>
			<div class="form-group col-md-6 h-100p" >
				<label for="">Code</label>
				<input type="text" class="form-control" v-model="language.code" placeholder="ex:. FR">
				<span class="help-block" v-show="!fields.code.state" v-text="fields.name.msg"></span>
			</div>

			<div class="form-group col-md-6 h-100p active-options">
				<label for="" class="w100p">Active</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input   @change="checkActive(1)" type="checkbox"  
					:checked="language.active == 1">
					<label for="checkbox1">YES

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input :checked="language.active != 1" type="checkbox"  @change="checkActive(0)" >
					<label for="checkbox1">NO 

					</label>
				</div>
			</div>
			<div class="form-group col-md-6 h-100p active-options">
				<label for="" class="w100p">Default</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input  :checked="language.default == 1"  @change="checkDefault(1)"  type="checkbox"  >
					<label for="checkbox1">YES

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input :checked="language.default != 1" type="checkbox"  @change="checkDefault(0)" >
					<label for="checkbox1">NO 

					</label>
				</div>
			</div>
			<div class="form-controls tar">
				<button type="button" @click="dashboard()" class="btn btn-danger">CANCEL</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
			</div>
		</form>


	</view-admin-languages-edit>
