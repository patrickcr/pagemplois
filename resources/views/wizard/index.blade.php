<wizard inline-template
        :advertisement="{{ $advertisement->toJson() }}"
        :step="0"
        step-size-label="Pick a Size and Template"
        step-edition-label="Edit your Advertisment"
        header-text="Le Titre de l'annonce"
        content-text="Le Texte de l'annonce"
        footer-text="Autres Infomations"
        :categories=" {{  json_encode($advertisement->Categories_Indexes()) }}"
        :locations="{{  json_encode($advertisement->Locations_Indexes()) }}"
        :contracts_types="{{  json_encode($advertisement->Contract_Types_Indexes()) }}"
        :tva="{{ $settings->db->tva   }}"
        workflow="{{ $workflow }}">

    <div class="wizard col-xs-12 no-padding">

        <legend> @{{ stepLabel }}</legend>
        {{--Step--}}
        <div class="step-wrapper" v-bind:class="stepClass">
            {{--Step Size--}}
            <div class="step size-template">
                <!-- Sizes -->
                <div class="col-md-1-3 no-padding templates antiscroll-wrap">
                    <div class="antiscroll-inner template-wrapper no-select">
                        @foreach($sizes as $size)
                            <div class="template size" v-on:click="changeSize({{$size->toJson()}})"
                                 v-bind:class="advertisement.size.id == {{ $size->id}} ? 'active' : ''">
                                <div class="header">
                                    <strong>Size</strong>&nbsp;{{ $size->name }}
                                </div>
                                <div class="title">
                                    {{ $size->name }}
                                </div>
                                <div class="footer dimensions">
                                    <img src="../../images/icons/dimensions.png" alt="dimensions"/>
                                    <span>120cm x 100cm</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Templates -->
                <div class="col-md-1-3 no-padding templates antiscroll-wrap">
                    <div class="antiscroll-inner template-wrapper no-select">

                        {{--TEMPLATE header-content-footer--}}
                        <div class="template header-content-footer"
                             v-bind:class="advertisement.structure == '' ? 'active': ''"
                             v-on:click="changeTemplate('')">
                            <div class="header">TITRE DE L'ANNONCE</div>
                            <div class="content">DESCRIPTION </br> DE </br> VOTRE ANNONCE</div>
                            <div class="footer">BAS DE L'ANONCE</div>
                        </div>

                        {{--TEMPLATE logo-header-content--}}
                        <div class="template logo-header-content"
                             v-bind:class="advertisement.structure == 'header-logo' ? 'active': ''"
                             v-show="advertisement.size.name !='1x1' &&  advertisement.size.name !='1x2' && advertisement.size.name !='1x3' && advertisement.size.name !='2x1'"
                             v-on:click="changeTemplate('header-logo')">
                            <div class="logo"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
                            <div class="header">VOTRE TITRE</div>
                            <div class="content">DESCRIPTION </br> DE </br> VOTRE ANNONCE</div>
                        </div>

                        {{--TEMPLATE header-content--}}
                        <div class="template header-content" v-on:click="changeTemplate('full-header')"
                             v-bind:class="advertisement.structure == 'full-header' ? 'active': ''">
                            <div class="header">TITRE DE L'ANNONCE</div>
                            <div class="content">DESCRIPTION </br> DE </br> VOTRE ANNONCE</div>
                        </div>

                        {{--TEMPLATE content-footer --}}
                        <div class="template content-footer" v-on:click="changeTemplate('full-footer')"
                             v-bind:class="advertisement.structure == 'full-footer' ? 'active': ''">
                            <div class="content">DESCRIPTION </br> DE </br> VOTRE ANNONCE</div>
                            <div class="footer">BAS DE L'ANONCE</div>
                        </div>

                        {{--TEMPLATE full-content--}}
                        <div class="template full-content" v-bind:click="changeTemplate('full')"
                             v-bind:class="advertisement.structure == 'full' ? 'active': ''">
                            <div class="content">
                                FULL PAGE POUR</br> VOTRE ANNONCE</br>
                            </div>
                        </div>

                        {{--TEMPLATE full-image--}}
                        <div class="template full-image" v-on:click="changeTemplate('image-upload')"
                             v-bind:class="advertisement.structure == 'image-upload' ? 'active': ''">
                            <div class="content">
                                FULL IMAGE</br>
                                <i class="fa fa-picture-o" aria-hidden="true"></i></br>UPLOAD IMAGE
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-7-4 template-preview" style="width: 74%;">

                    {{--ADVERTISMENT SIZE & TEMPLATE PREVIEW--}}
                    <div class="ads" v-bind:class="[advertisement.size.class, advertisement.structure]">

                        {{--logo--}}
                        <img v-show="advertisement.logoActive" class="logo" src="../../images/icons/picture.png"/>

                        {{--Header--}}
                        <div class="header" v-bind:style="advertisement.headerStyle" v-show="advertisement.headerActive"
                             v-html="advertisement.headerText"></div>
                        {{--Content--}}
                        <div class="content" v-bind:style="advertisement.contentStyle"
                             v-show="advertisement.contentActive"
                             v-html="advertisement.contentText"></div>
                        {{--Footer--}}
                        <div class="footer" v-bind:style="advertisement.footerStyle" v-show="advertisement.footerActive"
                             v-html="advertisement.footerText"></div>

                        {{--Upload Image--}}
                        <div v-show="advertisement.backgroundActive && advertisement.backgroundUrl== null"
                             class="image-upload">
                            <div class="image-ico"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
                            <button type="button" class="btn btn-primary">UPLOAD IMAGE</button>
                        </div>

                    </div>

                </div>
            </div>
            {{--End Step Size--}}

            {{--Step Edition--}}
            <div class="step edition-template">
                <div class="{{ Auth::User()->isAdmin() ? 'col-xs-52' : 'col-xs-47-1' }} edition">
                    {{--Accordion section edition--}}
                    <div id="accordion" class="panel-group">
                        {{--Header Edition Tab--}}
                        <div class="panel panel-default" v-show="advertisement.headerActive">
                            <div class="panel-heading">
                                <span data-toggle="collapse" data-parent="#accordion" href="#collapseHeader">EDIT THE TITLE</span>
                                <div id="header-bg" class="input-group colorpicker-component">

                                     <input type="text" value="#00AABB" class="form-control color-picker-input"
                                     placeholder="Background Color" />

                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                            <div id="collapseHeader" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ckeditor id="headerText" name="headerText"
                                              :field.sync="advertisement.headerText"
                                              toolbar="{{ Auth::User()->isAdmin() ? 'source' : 'basic' }}"
                                              heigth="300"
                                    ></ckeditor>
                                </div>
                            </div>
                        </div>
                        {{--Content Edition Tab--}}
                        <div class="panel panel-default" v-show="advertisement.contentActive">
                            <div class="panel-heading">
                                <span data-toggle="collapse" data-parent="#accordion" href="#collapseContent">
                                    EDIT THE CONTENT</span>
                                <div id="content-bg" class="input-group colorpicker-component">
                                    <span class="title">BACKGROUND COLOR:</span>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                            <div id="collapseContent" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ckeditor id="contentText" name="contentText"
                                              :field.sync="advertisement.contentText"
                                              toolbar="{{ Auth::User()->isAdmin() ? 'source' : 'basic' }}"
                                              heigth="300"
                                    ></ckeditor>
                                </div>
                            </div>
                        </div>
                        {{--Footer Edition tab--}}
                        <div class="panel panel-default" v-show="advertisement.footerActive">
                            <div class="panel-heading">
                                <span data-toggle="collapse" data-parent="#accordion" href="#collapseFooter">
                                    EDIT THE FOOTER</span>
                                <div id="footer-bg" class="input-group colorpicker-component">
                                    <span class="title">BACKGROUND COLOR:</span>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                            <div id="collapseFooter" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ckeditor id="footerText" name="footerText"
                                              :field.sync="advertisement.footerText"
                                              toolbar="{{ Auth::User()->isAdmin() ? 'source' : 'basic' }}"
                                              heigth="300"
                                    ></ckeditor>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--Advertisment Preview Column--}}
                <div class="{{ Auth::User()->isAdmin() ? 'col-xs-48' : 'col-xs-52-9' }}  template-preview">

                    <div class="ads" v-bind:class="[advertisement.size.class, advertisement.structure]">
                        {{--Logo--}}
                        <img v-show="advertisement.logoActive" class="logo" src="../../images/icons/picture.png"/>
                        {{--Header--}}
                        <div class="header" v-bind:style="advertisement.headerStyle" v-show="advertisement.headerActive"
                             v-html="advertisement.headerText"></div>
                        {{--Content--}}
                        <div class="content" v-bind:style="advertisement.contentStyle"
                             v-show="advertisement.contentActive"
                             v-html="advertisement.contentText"></div>
                        {{--Footer--}}
                        <div class="footer" v-bind:style="advertisement.footerStyle" v-show="advertisement.footerActive"
                             v-html="advertisement.footerText"></div>

                        {{--Image Upload--}}
                        <div v-show="advertisement.backgroundActive && advertisement.backgroundUrl== null"
                             class="image-upload">
                            <div class="image-ico"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
                            <button type="button" class="btn btn-primary">UPLOAD IMAGE</button>
                        </div>
                    </div>
                </div>
            </div>
            {{--End Step Edition --}}

            {{--Step Options--}}
            <div class="step options-template" v-if="this.advertisement.type == 1">
                {{--Column Type Avertisment & Type of Contract--}}
                <div class="col-xs-4">
                    {{--Panel Type Advertisement--}}
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">TYPE D'ANNONCE</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div v-on:click="advertisement.application=0, advertisement.offer=1"
                                     class="checkbox abc-checkbox abc-checkbox-success abc-checkbox-circle">
                                    <input type="checkbox" :checked="advertisement.offer == 1"
                                           v-on:click="advertisement.application=0, advertisement.offer=1">
                                    <label for="checkbox1">OFFRE D'EMPLOI

                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 no-padding">
                                <div v-on:click="advertisement.application=1, advertisement.offer=0"
                                     class="checkbox abc-checkbox abc-checkbox-info abc-checkbox-circle">
                                    <input :checked="advertisement.application == 1" type="checkbox"
                                           v-on:click="advertisement.application=1, advertisement.offer=0">
                                    <label for="checkbox1">DEMANDE D'EMPLOI

                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 no-padding">

                                <div v-if="advertisement.offer==1"><strong>Recevoir alerts email á chaque nouvelle
                                        demande 'emploi ? </strong>
                                    <div v-on:click="advertisement.mail_applications=1, advertisement.mail_offers=0"
                                         class="checkbox abc-checkbox abc-checkbox-primary">
                                        <input :checked="advertisement.mail_applications == 1"
                                               v-on:click="advertisement.mail_applications=1, advertisement.mail_offers=0"
                                               type="checkbox">
                                        <label for="checkbox1">RECEVOIR EMAIL ALERTS

                                        </label>
                                    </div>
                                </div>

                                <div v-if="advertisement.application==1"><strong>Recevoir alerts email á chaque nouvelle
                                        offre
                                        ? </strong>
                                    <div v-on:click="advertisement.mail_applications=0, advertisement.mail_offers=1"
                                         class="checkbox abc-checkbox abc-checkbox-primary">
                                        <input :checked="advertisement.mail_offers == 1"
                                               v-on:click="advertisement.mail_applications=0, advertisement.mail_offers=1"
                                               type="checkbox">
                                        <label for="checkbox1">RECEVOIR EMAIL ALERTS

                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--Panel Type of Contracts--}}
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">TYPE CONTRACT</h3>
                        </div>
                        <div class="panel-body">
                            <tree inline-template :dataset="{{ $contract_types }}" :checked.sync="contracts_types">
                                <ul class="tree">
                                    <tree-item :model="item" v-for="item in treeData" lazy></tree-item>
                                </ul>
                            </tree>
                        </div>
                    </div>
                </div>
                {{--Column Categories--}}
                <div class="col-xs-4">
                    <div class="panel panel-primary min-h-420">
                        <div class="panel-heading">
                            <h3 class="panel-title">CATEGORIES</h3>
                        </div>
                        <div class="panel-body">
                            <tree inline-template :dataset="{{ $categories }}" :checked.sync="categories">
                                <ul class="tree">
                                    <tree-item :model="item" v-for="item in treeData" lazy></tree-item>
                                </ul>
                            </tree>
                        </div>
                    </div>
                </div>
                {{--Column Locations--}}
                <div class="col-xs-4">
                    <div class="panel panel-primary min-h-420">
                        <div class="panel-heading">
                            <h3 class="panel-title">LOCATIONS</h3>
                        </div>
                        <div class="panel-body">
                            <tree inline-template :dataset="{{ $locations }}" :checked.sync="locations">
                                <ul class="tree">
                                    <tree-item :model="item" v-for="item in treeData" lazy></tree-item>
                                </ul>
                            </tree>
                        </div>
                    </div>
                    {{--End Step Options--}}
                </div>
            </div>
            {{--End Step Options--}}

            {{--Step Preview & Publish--}}
            <div class="step preview-publish">
                <div class="col-xs-52-9 template-preview">

                    <div class="ads" v-bind:class="[advertisement.size.class, advertisement.structure]">
                        {{--Logo--}}
                        <img v-show="advertisement.logoActive" class="logo" src="../../images/icons/picture.png"/>
                        {{--Header--}}
                        <div class="header" v-bind:style="advertisement.headerStyle" v-show="advertisement.headerActive"
                             v-html="advertisement.headerText"></div>
                        {{--Content--}}
                        <div class="content" v-bind:style="advertisement.contentStyle"
                             v-show="advertisement.contentActive"
                             v-html="advertisement.contentText"></div>
                        {{--Footer--}}
                        <div class="footer" v-bind:style="advertisement.footerStyle" v-show="advertisement.footerActive"
                             v-html="advertisement.footerText"></div>

                        {{--Image Upload--}}
                        <div v-show="advertisement.backgroundActive && advertisement.backgroundUrl== null"
                             class="image-upload">
                            <div class="image-ico"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
                            <button type="button" class="btn btn-primary">UPLOAD IMAGE</button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-47-1 edition">

                    <table v-if="this.advertisement.type == 1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Product Details</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Advertisement - Size @{{ advertisement.size.name }}</td>
                            <td>1 unit (8 Days)</td>
                            <td>@{{ parseFloat(advertisement.size.price).toFixed(2) }}</td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td class="border-transparent"></td>
                            <td>Subtotal</td>
                            <td>@{{ parseFloat(advertisement.size.price).toFixed(2) }}</td>
                        </tr>
                        <tr>
                            <td class="border-transparent"></td>
                            <td>TVA</td>
                            <td>@{{ tva}} %</td>
                        </tr>
                        <tr>
                            <td class="border-transparent"></td>
                            <td><strong>Total</strong></td>
                            <td>@{{ (price) }} {{ $settings->db->currency }}</td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="alert alert-dismissible alert-info"
                         v-if="advertisement.size.price == 0 && this.advertisement.type == 1">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Heads up!</strong> This <a href="#" class="alert-link"></a>
                        Your Advertisement if <strong>Free</strong> o charge, you can publish now

                    </div>

                    <div class="alert alert-dismissible alert-warning"
                         v-if="advertisement.size.price > 0 && this.advertisement.type == 1">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Heads up!</strong> This <a href="#" class="alert-link"></a>
                        Your Advertisement costs <strong>@{{ price}} {{ $settings->db->currency }} &nbsp;</strong>
                        you can Pay Now using Paypal, or Later by clicking on publish.

                    </div>

                    <div class="col-xs-12 tar" v-if="advertisement.size.price == 0 && this.advertisement.type == 1">

                        <button type="button" class="btn btn-primary" v-on:click="save(0)">Publish</button>
                    </div>

                    <div class="col-xs-12 tar" v-if="advertisement.size.price > 0 && this.advertisement.type == 1">
                        <button type="button" class="btn btn-default btn-paypal" v-on:click="save(1)">Publish and Pay
                            with Paypal
                        </button>
                        <button type="button" class="btn btn-primary" v-on:click="save(0)">Publish and Pay Later
                        </button>
                    </div>

                    <div class="col-xs-6" v-if="this.advertisement.type > 1">
                        <label class="w100p"><strong>SET THE LANGUAGE</strong></label>
                        <div class="btn-group">
                            @foreach($languages as $language)
                                <button type="button" class="btn btn-primary"
                                        v-on:click="ChangeLanguage({{$language->id }})"
                                        v-bind:class="advertisement.language_id== {{ $language->id }} ? 'active' : ''">{{ $language->name }}</button>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xs-6 tar" v-if="this.advertisement.type > 1">
                        <label class="w100p"><strong>SET STATUS</strong></label>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary"
                                    v-on:click="status = 1, advertisement.status = 1"
                                    v-bind:class="advertisement.status==1 ? 'active' : ''">ENABLE
                            </button>
                            <button type="button" class="btn btn-primary"
                                    v-on:click="status = 0,advertisement.status = 0"
                                    v-bind:class="advertisement.status==0 ? 'active' : ''">DISABLE
                            </button>

                        </div>
                    </div>

                    <div class="col-xs-12 tar" v-if="this.advertisement.type > 1">
                        </br>
                        </br>
                        <label class="w100p"><strong>PUBLISH YOUR ADVERTISEMENT</strong></label>
                        <button type="button" class="btn btn-primary" v-on:click="save(0)">Publish</button>
                    </div>


                </div>

            </div>
            {{--End Step Preview & Publish--}}

            {{--Final--}}
            {{--<div class="step final">--}}
            {{--</div>--}}
            {{--End Step Final--}}
        </div>
        {{--Controls--}}
        <div class="bottom-bar col-xs-12 no-padding">
            <div class="status-bar col-xs-6">
            </div>
            <div class="controls col-xs-6">
                <button type="button" v-on:click="cancel()" class="btn btn-default">Cancel</button>
                <button type="button" v-on:click="prev()" class="btn btn-primary">Previous</button>
                <button type="button" v-on:click="next()"
                        v-if="(advertisement.type==2 && step == 2) == false && (advertisement.type==1 && step == 3) == false"
                        class="btn btn-primary">Next
                </button>
                <button type="button" v-on:click="cancel()"
                        v-if="(advertisement.type==2 && step == 2) || (advertisement.type==1 && step == 3)"
                        class="btn btn-danger">Close
                </button>
            </div>
        </div>
    </div>

</wizard>