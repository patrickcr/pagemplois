<view-admin-words-edit inline-template :word="{{ $word->toJson() }}">


	<form @submit="save" method="POST" role="form">

		<legend>
			<span v-if="word.id > 0">EDIT WORD</span>
			<span v-if="word.id == -1">NEW WORD</span>
			<strong>@{{ word.reference }}</strong></legend>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.reference.class">
				<label for="">Reference</label>
				<input type="text" class="form-control" v-model="word.reference" placeholder="ex:. Advertisment">
				<span class="help-block" v-show="!fields.reference.state" v-text="fields.reference.msg"></span>
			</div>
			<div class="form-group col-md-6 h-100p" v-bind:class="fields.value.class">
				<label for="">Value</label>
				<input type="text" class="form-control" v-model="word.value" placeholder="ex:. advertisment">
				<span class="help-block" v-show="!fields.value.state" v-text="fields.value.msg"></span>
			</div>
			<div class="form-group col-xs-12">
				 <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">

                            <li v-for="translation in word.translations" v-bind:class="$index== 0 ? 'active' : ''">
                            <a v-bind:href="'#tab-'+translation.language_id" data-toggle="tab">@{{ translation.language.name }}</a></li>
                            
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div v-for="translation in word.translations" v-bind:class="$index== 0 ? 'active' : ''"

                        class="tab-pane" v-bind:id="'tab-'+translation.language_id">

                        	<input type="text" class="form-control" v-model="translation.value"   v-bind:placeholder="'Type translation in ' +  translation.language.name">

                        </div>
                        
                    </div>
                </div>
            </div>


			</div>

			<div class="form-controls tar">
				<button type="button" @click="dashboard()" class="btn btn-danger">CANCEL</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
			</div>
		</form>


	</view-admin-words-edit>
