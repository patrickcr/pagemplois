<view-admin-users inline-template>

  <div class="list-group">
    {{-- HEADER TITLES --}}
    <div class="list-group-item item-table header sky-bg">
      <div class="col-md-1">#</div>
      <div class="col-md-5">Reference</div>
      <div class="col-md-4">Value</div>
      <div class="col-md-2 tar">
       <button @click="getView('words/-1/edit')" type="button" class="btn btn-primary btn-sm btn-ico"><i class="fa fa-plus" aria-hidden="true"></i> NEW</button>

     </div>
   </div>
   {{-- HEADER FILTERS --}}
   <div class="list-group-item item-table header">
    <div class="col-md-1"></div>
    <div class="col-md-5">
      <input type="text" v-model="params.filters.reference" v-on:keyup="rebind()"></input>
    </div>
    <div class="col-md-4"><input type="text" v-model="params.filters.value" v-on:keyup="rebind()"></input></div>
    <div class="col-md-2"></div>
  </div>
  {{-- DATA --}}

  <div  v-for="item in data" class="list-group-item item-table">
    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
    <div class="col-md-5 dinamic"><span class="m-title">REFERENCE: </span>@{{ item.reference }}</div>
    <div class="col-md-4 dinamic"><span class="m-title">VALUE: </span>@{{ item.value }}</div>
   
    <div class="col-md-2 tar item-controls">
      <button @click="getView('words/' + item.id + '/edit')" type="button" class="btn btn-sm btn-primary">
        <i class="fa fa-pencil" aria-hidden="true"></i>
      </button>
      <button @click="remove(item.id)" type="button" class="btn btn-sm btn-danger">
        <i class="fa fa-trash" aria-hidden="true"></i>
      </button>
    </div>
  </div>

  {{-- FOOTER --}}
  <div class="list-group-item list-group-item-primary item-table sky-bg">
    <div class="col-xs-12 col-md-4">

      <div class="btn-group">
        <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''" @click="changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
      </div>


    </div>
    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
      <div v-if="page.pages.length > 1" class="btn-group">
        <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages" @click="changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
      </div>
    </div>
  </div>
</div>



</view-admin-users>