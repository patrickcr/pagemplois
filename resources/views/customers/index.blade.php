@extends('master')

@section('content')
    <view-customers-index inline-template>
        <div class="col-xs-12 no-padding">
            <h4 class="inline"><strong>Welcome</strong>, {{ Auth::User()->name }}</h4>
            <div class="col-xs-12 col-md-3 float-right-md no-padding">
                <a type="button" href="advertisements/new" class="btn btn-info btn-ico">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    CREATE NEW ADVERTISMENT</a>
            </div>
            <br/>

            <br/>
            <br/>
            <div class="col-xs-12 no-padding">
                <div class="alert alert-dismissible alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Heads up!</strong> This <a href="#" class="alert-link"></a>This is your dashboard page, here
                    you
                    can
                    view
                    all your advertisement and notifications, of course you can manage it to.
                    There is a few symbols you have to consider when looking to your advertisments.
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-md-8 no-padding">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">My Advertisments</h3>
                </div>
                <div class="panel-body">
                    <div class="list-group">
                        <div v-for="advertisement in advertisements" class="list-group-item item-advertisement row">

                            <div class="col-xs-12 col-md-6 modified no-padding">
                                <strong>Modified:</strong>&nbsp; @{{ advertisement.updated_at  | moment "dddd, MMMM Do YYYY" }}
                            </div>
                            <div class="col-xs-12 col-md-6 status no-padding">
                                <strong>Status:</strong>
                                <span v-if="advertisement.status == 1" class="label label-success margin-left-10 fs-12">Published</span>
                                <span v-if="advertisement.status == 2" class="label label-info  margin-left-10 fs-12">Waiting Review</span>
                                <span v-if="advertisement.status == 3" class="label label-warning  margin-left-10 fs-12">Waiting Payement</span>
                                <span v-if="advertisement.status == 4" class="label label-danger  margin-left-10 fs-12">Expired</span>
                            </div>
                            <div class="col-xs-12 col-md-10 no-padding">
                                @{{ advertisement.headerText | text }}
                            </div>
                            <div class="col-xs-12 col-md-2 no-padding tar">
                                <a v-bind:href="'advertisements/' + advertisement.id" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 special-padding-null">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">My Notifications</h3>
                </div>
                <div class="panel-body">
                    Panel body ...
                </div>
            </div>
        </div>
    </view-customers-index>
@endsection