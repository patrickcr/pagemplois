@extends('master')

@section('content')
    <div class="col-xs-12 no-padding">
        <h4 class="inline"><strong>Welcome to the Advertisement Tool</h4>
        <br/>
        <br/>
        <div class="col-xs-12 noty relative no-padding">
            <noty v-for="noty in notifications" :noty="noty"></noty>
        </div>

        <div class="alert alert-dismissible alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>hans On!</strong> This <a href="#" class="alert-link"></a>Where you can create your advertisment, note there is diferent sizes. You can create a free advertisment by using the size
            <strong>1x1</strong>, Any other size have a small cost betwen 1 and 5 $ and you can pay usin paypal for now, that will help us to maitain our platform. Enjoy !
        </div>
    </div>

    {!! $wizard !!}

@endsection