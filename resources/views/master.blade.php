<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pagemplois</title>
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="../../css/app.css">
    {{-- PLugins --}}
    <link rel="stylesheet" type="text/css" href="../../plugins/antiscroll-master/antiscroll.css">
</head>
<script>
    window.Laravel =
            <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>

    var z = null;
</script>
<body>
<search-index inline-template>
    <div class="container no-padding">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        <strong>PAGEMPLOIS</strong></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    {{--<ul class="nav navbar-nav">--}}
                    {{--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>--}}
                    {{--<li><a href="#">Link</a></li>--}}

                    {{--</ul>--}}
                    <form class="navbar-form navbar-center search-form" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Type your seach here">
                        </div>
                        <button type="submit" class="btn btn-default btn-search"><i class="fa fa-search"
                                                                                    aria-hidden="true"></i></button>
                    </form>
                    @include('auth.user_control')
                </div>
            </div>
        </nav>
        <div class="container master-content" style="width:100%;">
            @yield('content')
        </div>
        <footer class="no-padding">
            <div>
                <ul>
                    <li>TE</li>
                </ul>
            </div>
        </footer>
    </div>
</search-index>
</body>
{{-- Plugins --}}

<script type="text/javascript" src="../../plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../js/app.js"></script>

</html>