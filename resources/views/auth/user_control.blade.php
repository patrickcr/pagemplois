<ul class="nav navbar-nav navbar-right user-control">
  @if(Auth::check())
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
      <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
      <li>
        <a href="#">
          <div>
            <i class="fa fa-comment fa-fw"></i> New Comment
            <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="#">
          <div>
            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
            <span class="pull-right text-muted small">12 minutes ago</span>
          </div>
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="#">
          <div>
            <i class="fa fa-envelope fa-fw"></i> Message Sent
            <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="#">
          <div>
            <i class="fa fa-tasks fa-fw"></i> New Task
            <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="#">
          <div>
            <i class="fa fa-upload fa-fw"></i> Server Rebooted
            <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
        </a>
      </li>
      <li class="divider"></li>
      <li>
        <a class="text-center" href="#">
          <strong>See All Alerts</strong>
          <i class="fa fa-angle-right"></i>
        </a>
      </li>
    </ul>
    <!-- /.dropdown-alerts -->
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><strong>Welcome</strong>,&nbsp;&nbsp;{{ Auth::User()->name}} 
      <img class="user-logo" src="../../images/icons/no_avatar.png" alt="user-ico"></img>
      <span  class="caret"></span></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="/dashboard">Dashboard</a></li>
        <li><a href="#">Profile</a></li>
        @if(Auth::User()->hasRole(['admin']))
        <li class="divider"></li>
        <li><a href="/admin">Admin</a></li>
        @endif
        <li class="divider"></li>
        <li class="logout"><a href="/logout"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
      </ul>
    </li>
    @else
    <li><a href="login"><strong>Login</strong></a></li>
    @endif
  </ul>