@extends('master')

@section('content')
<auth inline-template>
	<div class="auth col-xs-12 no-padding">
		<div class="auth_wrapper" v-bind:class="state">
			<section class="login">
				<div class="col-xs-12  col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Login</h3>
						</div>
						<div class="panel-body">
						
						<form @submit="login" role="form">
							<div class="form-group" v-bind:class="fields.email.class">
								<label for="email" class="col-lg-2 control-label">Email</label>
								<div class="col-lg-10">
									<input type="email" id="email" class="form-control" v-model="model.email" placeholder="Email">
	<span class="help-block" v-show="!fields.email.state" v-text="fields.email.msg"></span>
								</div>
							</div>
							<div class="form-group" v-bind:class="fields.password.class">
								<label for="password" class="col-lg-2 control-label">Password</label>
								<div class="col-lg-10">
									<input type="password" id="password" class="form-control" v-model="model.password" placeholder="Password">
									<span  class="help-block" v-show="!fields.password.state" v-text="fields.password.msg"></span>
								</div>
							</div>
							<div class="col-xs-12 controls">
							<button type="button" class="btn btn-default">REGITER</button>
							<button type="submit" class="btn btn-primary">LOGIN</button>
							</div>
						</form>
					</div>
					</div>

				</div>

			</section>
			<section class="register">
				<div class="col-xs-12">

				</div>
			</section>
		</div>
	</div>
</auth>
@endsection



</div>