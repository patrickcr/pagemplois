<view-admin-users-edit inline-template :user="{{ $user->toJson() }}">

	<form @submit="save" method="POST" role="form">

		<legend>
			<span v-if="user.id > 0">EDIT USER</span>
			<span v-if="user.id == -1">NEW USER</span>
			<strong>@{{ user.name }}</strong></legend>
			<div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
				<label for="">Name</label>
				<input type="text" class="form-control" v-model="user.name" placeholder="ex:. Thomas Spycher">
				<span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
			</div>
			<div class="form-group col-md-3 h-100p active-options">
				<label for="" class="w100p">Active</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input   @change="user.active=1" type="checkbox"  
					:checked="user.active == 1">
					<label for="checkbox1">YES

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input :checked="user.active == 0" type="checkbox"  @change="user.active=0" >
					<label for="checkbox1">NO 

					</label>
				</div>
			</div>

			<div class="form-group col-md-3 h-100p active-options">
				<label for="" class="w100p">Account Type</label>
				<div   class="checkbox abc-checkbox abc-checkbox-primary abc-checkbox-circle">
					<input   @change="user.type=2" type="checkbox"  
					:checked="user.type == 2">
					<label for="checkbox1">CUSTOMER

					</label>
				</div>
				<div class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
					<input :checked="user.type == 1" type="checkbox"  @change="user.type=1" >
					<label for="checkbox1">ADMIN 

					</label>
				</div>
			</div>

			<div class="form-group col-md-6 h-100p" v-bind:class="fields.email.class">
				<label for="">Email</label>
				<input type="email" class="form-control" v-model="user.email" placeholder="ex:. thomas.spycher@pagemplois.com">
				<span class="help-block" v-show="!fields.email.state" v-text="fields.email.msg"></span>
			</div>


			<div class="form-group col-md-6 h-100p" v-bind:class="fields.password.class">
				<label for="">Password</label>
				<input type="password" class="form-control" v-model="user.password" placeholder="******">
				<span class="help-block" v-show="!fields.email.state" v-text="fields.password.msg"></span>
			</div>


			<div class="form-controls tar">
				<button type="button" @click="dashboard()" class="btn btn-danger">CANCEL</button>
				<button type="submit" class="btn btn-primary">SAVE</button>
			</div>
		</form>


	</view-admin-users-edit>
