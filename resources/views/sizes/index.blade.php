<view-admin-sizes inline-template>

    <div class="list-group">
        {{-- HEADER TITLES --}}
        <div class="list-group-item item-table header sky-bg">
            <div class="col-md-1">#</div>
            <div class="col-md-3">Name</div>
            <div class="col-md-3">Class</div>
            <div class="col-md-2 tac">Active</div>
            <div class="col-md-1 tac">Price</div>
            <div class="col-md-2 tar">
                <button v-on:click="getView('sizes/-1/edit')" type="button" class="btn btn-primary btn-sm btn-ico"><i
                        class="fa fa-plus" aria-hidden="true"></i> NEW</button>

            </div>
        </div>
        {{-- HEADER FILTERS --}}
        <div class="list-group-item item-table header">
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <input type="text" v-model="params.filters.name" v-on:keyup="rebind()"></input>
            </div>
            <div class="col-md-3"><input type="text" v-model="params.filters.class" v-on:keyup="rebind()"></input>
            </div>
            <div class="col-md-2 tac">
                <div class="btn-group" role="group">
                    <a href="#" v-on:click="changeActive()" v-bind:class="classes.active.all" class="btn btn-xs btn-default"
                    >ALL</a>
                    <a href="#" v-on:click="changeActive(1)" v-bind:class="classes.active.on" class="
                    btn btn-xs btn-success btn-default">ON</a>
                    <a href="#" v-on:click="changeActive(0)" v-bind:class="classes.active.off" class="
                    btn btn-xs btn-danger btn-default">OFF</a>
                </div>
            </div>
            <div class="col-md-1">
                <input type="text" v-model="params.filters.price" class="form-input wp100" v-on:keyup="rebind()">
            </div>

            <div class="col-md-2"></div>
        </div>
        {{-- DATA --}}

        <div v-for="item in data" class="list-group-item item-table">
            <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
            <div class="col-md-3 dinamic"><span class="m-title">NAME: </span>@{{ item.name }}</div>
            <div class="col-md-3 dinamic"><span class="m-title">CLASS: </span>@{{ item.class }}</div>
            <div class="col-md-2 tac dinamic">
                <span class="m-title">STATUS: </span>
                <span v-if="item.active" class="label label-success">ON</span>
                <span v-if="item.active == 0" class="label label-danger">OFF</span>
            </div>
            <div class="col-md-1 tac dinamic">
                <span class="m-title">STATUS: </span>
                <span v-if="item.price == 0" class="label label-success">@{{ item.price }} {{ $settings->db->currency }}</span>
                <span v-if="item.price > 0" class="label label-info">@{{ item.price }} {{ $settings->db->currency }}</span>

            </div>
            <div class="col-md-2 tar item-controls">
                <button v-on:click="getView('sizes/' + item.id + '/templates')" type="button"
                        class="btn btn-sm btn-info">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                </button>

                <button v-on:click="getView('sizes/' + item.id + '/edit')" type="button"
                        class="btn btn-sm btn-primary">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </button>
                <button v-on:click="remove(item.id)" type="button" class="btn btn-sm btn-danger">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        {{-- FOOTER --}}
        <div class="list-group-item list-group-item-primary item-table sky-bg">
            <div class="col-xs-12 col-md-4">

                <div class="btn-group">
                    <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''" v-on:click="
                    changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                </div>


            </div>
            <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                <div v-if="page.pages.length > 1" class="btn-group">
                    <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages" v-on:click="
                    changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                </div>
            </div>
        </div>
    </div>


</view-admin-sizes>