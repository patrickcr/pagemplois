<view-admin-sizes-templates inline-template :size="{{ $size->toJson() }}" :languages="{{ $languages->toJson() }}">
    <div class="col-xs-12 no-padding">
        <h4 class="inline"><strong>TEMPLATES FOR </strong> {{ $size->name }}</h4>
        <div class="col-xs-12 col-md-3 float-right-md no-padding">
            <a type="button" v-on:click="getView('advertisements/-{{ $size->id }}/template');"
               class="btn btn-info btn-ico">
                <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                CREATE PUB ADVERTISMENT</a>
        </div>
        <br/>

        <br/>
        <br/>
        <div class="col-xs-12 no-padding">
            <div class="alert alert-dismissible alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Heads up!</strong> This is where you can create Advertismenet templates for the size
                <strong>{{ $size->name }}</strong>.
                <br/>
                You can create template in diferent languages. For now you can create Publicity advertisment for
                diferent Languages.
            </div>
        </div>
        <br/>
        <br/>
        <div class="col-xs-12 inline-block no-padding">


            <div class="panel panel-info" v-for="language in languages">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> @{{ language.name }}</strong></h3>
                </div>
                <div class="panel-body">

                    <div class="panel panel-default panel-advertisement"
                         v-for="advertisement in data | filterBy language.id in 'language_id'">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <div class="col-xs-4 no-padding">
                                    <span v-if="advertisement.type==2" class="label label-warning">PUB </span>
                                </div>
                                <div class="col-xs-4 no-padding">
                                    <div class="btn-group">
                                        <button type="button" v-bind:class="(advertisement.status == 1) ? 'active' : '' " class="btn btn-xs btn-success">ON</button>
                                        <button type="button" v-bind:class="(advertisement.status == 0) ? 'active' : '' " class="btn btn-xs btn-danger">OFF</button>
                                    </div>
                                </div>
                                <div class="col-xs-4 tar no-padding">
                                    <button type="button" v-on:click="remove(advertisement.id)"
                                            class="btn btn-xs btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <button v-on:click="getView('advertisements/' + advertisement.id +'/template');"
                                            type="button" class="btn btn-xs btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </button>

                                </div>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="ads" v-bind:class="[advertisement.size.class, advertisement.structure]">
                                {{--Logo--}}
                                <img v-show="advertisement.logoActive" class="logo"
                                     src="../../images/icons/picture.png"/>
                                {{--Header--}}
                                <div class="header" v-bind:style="advertisement.headerStyle"
                                     v-show="advertisement.headerActive"
                                     v-html="advertisement.headerText"></div>
                                {{--Content--}}
                                <div class="content" v-bind:style="advertisement.contentStyle"
                                     v-show="advertisement.contentActive"
                                     v-html="advertisement.contentText"></div>
                                {{--Footer--}}
                                <div class="footer" v-bind:style="advertisement.footerStyle"
                                     v-show="advertisement.footerActive"
                                     v-html="advertisement.footerText"></div>

                                {{--Image Upload--}}
                                <div v-show="advertisement.backgroundActive && advertisement.backgroundUrl== null"
                                     class="image-upload">
                                    <div class="image-ico"><i class="fa fa-picture-o" aria-hidden="true"></i></div>
                                    <button type="button" class="btn btn-primary">UPLOAD IMAGE</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <div class="col-xs-12 col-md-3 float-right-md no-padding">
                <a type="button" v-on:click="close()" class="btn btn-info">
                    CLOSE</a>
            </div>
        </div>
</view-admin-sizes-templates>