<view-admin-sizes-edit inline-template :size="{{ $size->toJson() }}">

    <form v-on:submit="save" role="form">
        <legend>
            <span v-if="size.id > 0">EDIT SIZE</span>
            <span v-if="size.id == -1">NEW SIZE</span>
            <strong>@{{ size.name }}</strong></legend>
        <div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
            <label for="">Name</label>
            <label for="">Name</label>
            <input type="text" class="form-control" v-model="size.name" placeholder="ex:. 1x1">
            <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
        </div>
        <div class="form-group col-md-6 h-100p" v-bind:class="fields.class.class">
            <label for="">Class</label>
            <input type="text" class="form-control" v-model="size.class" placeholder="ex:. Lausanne">
            <span class="help-block" v-show="!fields.class.state" v-text="fields.class.msg"></span>
        </div>

        <div class="form-group col-md-6 h-100p active-options">
            <label for="" class="w100p">Active</label>
            <div v-on:click="size.active =1"
                 class="checkbox abc-checkbox abc-checkbox-success abc-checkbox-circle">
                <input type="checkbox" :checked="size.active == 1" v-on:click="size.active=1">
                <label for="checkbox1">YES</label>
            </div>

            <div v-on:click="size.active =0"
                 class="checkbox abc-checkbox abc-checkbox-danger abc-checkbox-circle">
                <input type="checkbox" :checked="size.active == 0" v-on:click="size.active=0">
                <label for="checkbox1">NO</label>
            </div>
        </div>
        <div class="form-group col-md-6 h-100p">
            <label for="">Price</label>
            <input type="text" class="form-control" v-model="size.price" placeholder="ex:0">
        </div>
        <div class="form-controls tar">
            <button type="button" v-on:click="dashboard()" class="btn btn-danger">CANCEL</button>
            <button type="submit" class="btn btn-primary">SAVE</button>
        </div>
    </form>


</view-admin-sizes-edit>
