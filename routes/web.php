<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/*HOME*/
Route::get('/','HomeController@index')->name('home')->middleware('web');

/*CUTOMER DASHABORD*/
Route::get('dashboard', 'CustomersController@show')->name('dashboard')->middleware('role:admin|customer');

/*ADMIN*/
Route::get('admin', 'HomeController@admin')->middleware('role:admin');

/*AUTH*/
Route::get('login', 'AuthController@loginView')->middleware('web');
Route::post('login', 'AuthController@login')->name('login')->middleware('web');
Route::get('register', 'AuthController@registerView')->middleware('web');
Route::get('logout', 'AuthController@logout')->name('logout')->middleware('web');

/*USERS*/
Route::resource('users', 'UsersController');

/*ADVERTISMENTS*/
Route::get('advertisements/search', 'AdvertisementsController@search')->middleware('web');
Route::get('advertisements/{id}/template', 'AdvertisementsController@template')->middleware('role:admin');


/*ADVERTISMENTS CUSTOMERS*/
Route::get('advertisements/customer', 'CustomersController@customer')->middleware('role:admin|customer');
Route::get('advertisements/{id}', 'CustomersController@edit')->middleware('role:admin|customer');

/*SIZES*/
Route::resource('sizes', 'SizesController');
Route::get('sizes/{id}/templates', 'SizesController@templatesShow');
Route::get('sizes/templates/{id}', 'SizesController@templates');




/*CATEGORIES*/
Route::resource('categories', 'CategoriesController');

/*LOCATIONS*/
Route::resource('locations', 'LocationsController');

/*LANGUAGES*/
Route::resource('languages', 'LanguagesController');

/*WORDS*/
Route::resource('words', 'WordsController');

/*SETTINGS*/
Route::resource('settings', 'SettingsController');
