<?php

namespace App;

use App\Category;
use App\Dataset;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];
    protected $childrens = [];

    public function Advertisement()
    {
        return $this->belongsToMany('App\Advertisement', 'advertisement_categories');
    }


    public function childrens()
    {
        $this->childrens = new Collection();
        $this->getChildren($this->id);
        return $this->childrens;
    }

    public function getChildren($id)
    {
        $children_data = $this->where('parent', '=', $id)->get();
        foreach ($children_data as $children) {
            $this->childrens->add($children);
            $this->getChildren($children->id);
        }
    }

    public function deleteAll()
    {
        $childrens = $this->childrens();
        foreach ($childrens as $children)
            $children->deleteAll();
        $this->delete();
    }

    public function scopeEdit($query, $id)
    {
        if($id > 0)
            $category = Category::find($id); 
        else
        {
            $category = new Category();
            $category->id = -1;
            $category->name = "";
            $category->reference = "";
            $category->active = 1;
            $category->parent = null;
            $category->order = 0;
        }
        
        return $category;
    }

    public function scopeParent($query, $id)
    {
        
        return Category::where('id', '!=', $id)->get();
    }


    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        foreach ($filters as $key => $value) {


            if($value != '')
            { 
                $query = $query->where($key,'LIKE', '%'.$value.'%');   
            }
        }

        $sorts = $request->get("sorts");
        foreach ($sorts as $key => $value) {


            if($value != '')
            {

                $query = $query->OrderBy($key,$value);   

            }
        }


        return new Dataset($query, $skip, $take);

    }

    public function scopeActive($query)
    {
        return $query->where('active', '=', 1)->orderBy('parent', 'asc')->orderBy('order', 'asc')->get();
    }

    public function UI($item)
    {
        $childrens = Category::where('parent', '=', $item->id)->where('active', '=', 1)->get();
        if ($childrens->count() > 0) {
            $li = '<li class="check-item"> <i data-id="' . $item->id . '" class="fa fa-square-o check-control"></i><i class="fa fa-chevron-right" ';
            $li .= 'data-toggle="collapse" data-target="#c-' . $item->id . '"></i>';
            $li .= $item->name . '<ul class="collapse" id="c-' . $item->id . '">';
            foreach ($childrens as $child)
                $li .= $child->UI($child);
            $li .= '</ul></li>';
            return $li;
        } else
        return '<li class="check-item"><i data-id="' . $item->id . '" class="fa fa-square-o check-control"></i><div>' . $item->name . '</div></li>';
    }


}
