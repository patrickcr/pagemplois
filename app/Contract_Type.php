<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract_Type extends Model
{
    protected $table = 'contract_types';

    public  function Advertisement(){
        return $this->belongsToMany('App\Advertisement', 'adv_contract_types');
    }

    public function scopeActive($query){
    	return $query->where('active','=', 1)->get();
    }
}
