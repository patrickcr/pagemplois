<?php

namespace App\Words;

use App\FormResult;
use App\Translation;
use App\Validation\ValidateRequestsForApp;
use App\Word;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class WordBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();
        
    }


    public function messages(){

        return [
        'reference.required' => "La reference de la mot es necessaire",
        'reference.exists' => "Cette reference exist dejá",
        'value.required'  => 'La reference ce un champ necessaire'
        ];
    }

    public function validateRequest()
    {

      if($this->request->id == -1)
      {
        $this->validate($this->request, [
            'reference' => 'required|unique:words'
            ], $this->messages());

        
        }//$this->throwValidationException($request, $validator);


        return $this;

    }





    public function saveDB()
    {


        $result = new FormResult();

        try
        {

            if($this->request->id > 0)
                $word = Word::find($this->request->id);
            else
                $word = new Word();

            $word->reference = $this->request->reference;
            $word->value = $this->request->value;
            $word->save();

            if(count($this->request->translations> 0))
            { 
               $word->Translations()->delete();

               foreach($this->request->translations as $translation_data) {


                if(strlen($translation_data["value"])> 0)
                {
                    $translation  = new Translation(); 
                    $translation->value = $translation_data["value"];
                    $translation->language_id = $translation_data["language_id"];
                    $word->Translations()->save($translation);  
                } 

                    $word->save();
            }

        }



    }
    catch(Exception $ex)
    {

        $result->result = false;
    }

    return $result->toJson();

}

public function delete($id)
{
    $result = new FormResult();
    try{
        $word = Word::find($id);
        if($word != null)
            $word->delete();
    }
    catch(Exception $ex)
    {
        $result->result = false;
    }
    return $result;

}

}

?>