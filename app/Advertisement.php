<?php

namespace App;

use App\Size;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Advertisement extends Model
{
    protected $guarded = ['id', 'status'];
    protected $fillable = [];


    public function scopeNew($query)
    {
        $advertisement = new Advertisement();
        $advertisement->id = -1;
        $advertisement->type = 1;
        $advertisement->size_id = 1;
        $advertisement->offer = 1;
        $advertisement->mail_applications = 1;
        $advertisement->application = 0;
        $advertisement->mail_offers = 0;

        $advertisement->size = Size::first();
        $advertisement->structure = "";
        $advertisement->backgroundActive = false;
        $advertisement->backgroundUrl = null;
        $advertisement->logoActive = false;
        $advertisement->logoUrl = null;
        $advertisement->headerActive = 1;
        $advertisement->headerStyle = "";
        $advertisement->headerText = "<p style=\"text-align:center\">Votre titre ici</p>";
        $advertisement->contentActive = 1;
        $advertisement->contentText = "<p style=\"text-align:center\">Votre text ici</p>";
        $advertisement->contentStyle = "";
        $advertisement->footerActive = 1;
        $advertisement->footerText = "<p style=\"text-align:center\">Votre bas de page ici</p>";
        $advertisement->footerStyle = "";
        return $advertisement;
    }


    public function User()
    {
        return $this->belongsTo('App\User');
    }


    public function Size()
    {
        return $this->belongsTo('App\Size');
    }

    public function Payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function OrderAdvertisement()
    {
        return $this->hasMany('App\OrderAdvertisement');
    }

    public function Categories()
    {
        return $this->belongsToMany('App\Category', 'advertisement_categories');
    }


    public function Categories_Indexes()
    {
        $array = [];
        $categories = $this->Categories()->get(array('id'));


        foreach ($categories as $category)
            array_push($array, $category->id);

        return $array;
    }


    public function Locations()
    {
        return $this->belongsToMany('App\Location', 'advertisement_locations');
    }

    public function Locations_Indexes()
    {
        $array = [];
        $locations = $this->Locations()->get(array('id'));


        foreach ($locations as $location)
            array_push($array, $location->id);

        return  $array;
    }
    public function Contract_Types()
    {
        return $this->belongsToMany('App\Contract_Type', 'adv_contract_types', 'advertisement_id', 'contract_type_id');
    }

    public function Contract_Types_Indexes()
    {
        $array = [];
        $contract_types = $this->Contract_Types()->get(array('id'));


        foreach ($contract_types as $contract_type)
            array_push($array, $contract_type->id);

        return $array;
    }

    public function scopeByPaymentId($query, $paymentId)
    {
        return $query->where('paymentId', $paymentId)->get()->first();
    }

    public function scopeSkipTake($query, $params)
    {
        return $query->with('Size')->where('status', '=', 1)->skip($params['skip'])->take($params['take'])->get();
    }

    public function scopeTemplates($query, $size_id)
    {
        return $query->with('Size')->where("size_id", $size_id)->where('type', '>', 1)->get();
    }


    public function scopeCustomer($query)
    {
        return $query->with('Size')->where('user_id', Auth::User()->id)->get();
    }

    public function scopePub($query)
    {
        return $query->with('Size')->where('type', 2)->get();
    }

    public function scopePubBySize($query, $size_id)
    {
        return $query->with('Size')->where('size_id', $size_id)->where('status', 1)->where('type', 2)->get();
    }

    public function Bill()
    {

        $bill = new Bill();
        //push the advertisement as an item
        array_push($bill->itens, $this);
        $bill->name = $this->User->name;

        $bill->sub_total = floatval($bill->itens[0]->Size->price);
        $bill->tax = floatval(18 / 100) * $bill->sub_total;
        $bill->total = $bill->sub_total + $bill->tax;
        return $bill;

    }

    public function SavePayment($paypal_payement)
    {

        $payment = new \App\Payment();
        $payment->doc = $this->id . '-' . $this->Payments()->count();
        $payment->paymentId = $paypal_payement->id;
        $payment->cart = $paypal_payement->getCart();
        $payment->payement_type = $paypal_payement->getPayer()->payment_method;
        $paypal_payement->payerVerification = $paypal_payement->getPayer()->status;
        //Payer Info
        $payment->payerID = $paypal_payement->getPayer()->getPayerInfo()->payer_id;
        $payment->payerEmail = $paypal_payement->getPayer()->getPayerInfo()->email;
        $payment->payerName = $paypal_payement->getPayer()->getPayerInfo()->first_name . ' ' . $paypal_payement->getPayer()->getPayerInfo()->last_name;
        $payment->payerPhone = $paypal_payement->getPayer()->getPayerInfo()->phone;
        $payment->payerAddress = $paypal_payement->getPayer()->getPayerInfo()->billing_address;
        $payment->payerCountryCode = $paypal_payement->getPayer()->getPayerInfo()->country_code;
        $payment->status = $paypal_payement->state;
        //transaction details
        $payment->amount = $paypal_payement->transactions[0]->getAmount()->total;
        $payment->currency = $paypal_payement->transactions[0]->getAmount()->currency;

        //Save Payement
        $this->Payments()->save($payment);
    }
}
