<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\ServiceProvider;

class SettingsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('settings', new Settings\AppSettings());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Settings\AppSettings::class, function ($app) {
            $settings = new Settings\AppSettings();
            return new $settings;
        });
    }
}
