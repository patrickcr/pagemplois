<?php
/**
 * Created by PhpStorm.
 * User: patrick
 * Date: 11/10/16
 * Time: 11:43
 */

namespace App\Settings;


use App\Settings;

class AppSettings
{
    public $status = false;
    public $db = null;

    public function __construct()
    {
        //Get Settings from Database
        $this->db = Settings::App();

        //Check if settings are not null
        if ($this->db->count() > 0)
            $this->status = true;
        else
            $this->status = false;
    }
}