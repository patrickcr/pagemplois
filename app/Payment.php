<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];
    protected $table = 'advertisements_payements';

    public  function  Advertisement()
    {
        return $this->belongsToMany('App\Advertisement');
    }
}
