<?php

namespace App\Sizes;

use App\FormResult;
use App\Size;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;



class SizeBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();

    }


    public function messages(){

        return [
            'name.required' => "The name is required field",
            'name.exists' => "This name already exists",
            'class.required'  => 'The class is a required field',
            'class.exists' => "This class name already exisst",
        ];
    }

    public function validateRequest()
    {

        if($this->request->id == -1)
        {
            $this->validate($this->request, [
                'name' => 'required|unique:sizes',
                'reference' => 'required|unique:sizes'
            ], $this->messages());


        }//$this->throwValidationException($request, $validator);


        return $this;

    }





    public function saveDB()
    {


        $result = new FormResult();

        try
        {

            if($this->request->id > 0)
                $size = Size::find($this->request->id);
            else
                $size = new Size();

            $size->fill($this->request->all());

            $size->save();

        }
        catch(Exception $ex)
        {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try{
            $location = Location::find($id);
            if($location != null)
                $location->deleteAll();
        }
        catch(Exception $ex)
        {
            $result->result = false;
        }
        return $result;

    }

}

?>