<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{


    protected $guarded = [];
    protected $fillable = [];

    /**
     * @return existing instance of setti   prongs
     */
    public function scopeEdit($query)
    {
        $settings = Settings::all()->first();

        if (empty($settings)) {
            $settings = new Settings();
            $settings->name = "";
            $settings->slogan = "";
            $settings->email = "";
            $settings->support_email = "";
            $settings->no_reply_email = "";
            $settings->country = "";
            $settings->address = "";
            $settings->number = "";
            $settings->postcode = "";
            $settings->language = "";
            $settings->currency = "";
            $settings->tva = "";

        }
        return $settings;
    }

    public function scopeApp($query)
    {
        return Settings::all()->first();
    }
}