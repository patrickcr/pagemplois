<?php

namespace App\Advertisements;

use App\Advertisement;
use App\FormResult;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AdvertisementBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();

    }


    public function messages()
    {

        return [
            'advertisement.size_id' => 'required',
            'advertisement.advertisement.reference' => 'required',
            'advertisement.backgroundActive' => 'required',
            'advertisement.logoActive' => 'required',
            'advertisement.headerActive' => 'required',
            'advertisement.contentActive' => 'required',
            'advertisement.footerActive' => 'required',
            'advertisement.language_id' => 'required',
            'advertisement.size_id' => 'required',
            'advertisement.offer' => 'required',
            'advertisement.application' => 'required',
            'advertisement.mail_applications' => 'required',
            'advertisement.mail_offers' => 'required'
        ];
    }

    public function validateRequest()
    {


        $this->validate($this->request, [
            'advertisement.size_id' => 'required',
            'advertisement.backgroundActive' => 'required',
            'advertisement.logoActive' => 'required',
            'advertisement.headerActive' => 'required',
            'advertisement.contentActive' => 'required',
            'advertisement.footerActive' => 'required',
            'advertisement.size_id' => 'required',
            'advertisement.offer' => 'required',
            'advertisement.application' => 'required',
            'advertisement.mail_applications' => 'required',
            'advertisement.mail_offers' => 'required'

        ], $this->messages());


        return $this;

    }


    public function saveDB()
    {

        $result = new FormResult();

        try {
            if ($this->request->advertisement['id'] > 0)
                $advertisement = Advertisement::find($this->request->advertisement['id']);
            else
                $advertisement = new Advertisement();

            if ($advertisement->type == 2 && Auth::User()->isAdmin() == false)
                return redirect()->route('login');

            $advertisement->fill($this->request->advertisement);
            $advertisement->user_id = Auth::User()->id;

            //Check for advertisments TYPE == 2
            if ($advertisement->type == 2)
                $advertisement->status = $this->request->status;

            $advertisement->save();


            //Save Related data
            $advertisement->Categories()->detach();
            $advertisement->Categories()->sync($this->request->categories);


            $advertisement->Locations()->detach();
            $advertisement->Locations()->sync($this->request->locations);

            $advertisement->Contract_Types()->detach();
            $advertisement->Contract_Types()->sync($this->request->contracts_types);

            $advertisement = Advertisement::with('Size')->find($advertisement->id);
            $result->data = $advertisement;

        } catch (Exception $ex) {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try {
            $advertisement = Advertisement::find($id);
            if (!empty($advertisement) && $advertisement->count() > 0)
                $advertisement->delete();
        } catch (Exception $ex) {
            $result->result = false;
        }
        return $result;

    }

}

?>