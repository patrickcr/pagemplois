<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{

    protected $guarded = ['id'];
    protected $fillable = [];

    public function Advertisements()
    {
        return $this->hasMany('App\Advertisement');
    }


    public function PackageSizeConfigs()
    {
        return $this->hasMany('App\Package_Size_Config');
    }

    public function scopeActive($query)
    {
        return $query->where('active', '=', 1)->get();
    }

    public function scopeByName($query, $name)
    {
        return $query->where('name', $name)->get()->first();
    }


    public function scopeEdit($query, $id)
    {
        if ($id > 0)
            $size = Size::find($id);
        else {
            $size = new Size();
            $size->id = -1;
            $size->name = "";
            $size->class = "";
            $size->active = 1;
            $size->prive = 0.00;
        }

        return $size;
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        foreach ($filters as $key => $value) {


            if ($value != '') {
                $query = $query->where($key, 'LIKE', '%' . $value . '%');
            }
        }

        $sorts = $request->get("sorts");
        foreach ($sorts as $key => $value) {


            if ($value != '') {

                $query = $query->OrderBy($key, $value);

            }
        }

        return new Dataset($query, $skip, $take);

    }
}
