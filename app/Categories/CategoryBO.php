<?php

namespace App\Categories;

use App\Category;
use App\FormResult;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class CategoryBO
{
	use ValidateRequestsForApp;


	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function save()
	{

		return $this->validateRequest()->saveDB();

	}


    public function messages(){

        return [
        'name.required' => "Le nom de la categorie es necessaire",
        'name.exists' => "Cette categorie exist dejá",
        'reference.required'  => 'La reference ce un champ necessaire',
        'reference.exists' => "Cette reference exist dejá",
        ];
    }

    public function validateRequest()
    {

      if($this->request->id == -1)
      {
        $this->validate($this->request, [
            'name' => 'required|unique:categories',
            'reference' => 'required|unique:categories'
            ], $this->messages());


        }//$this->throwValidationException($request, $validator);


        return $this;

    }





    public function saveDB()
    {


        $result = new FormResult();

        try
        {

            if($this->request->id > 0)
                $category = Category::find($this->request->id);
            else
                $category = new Category();

            $category->fill($this->request->all());

            $category->save();

        }
        catch(Exception $ex)
        {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try{
            $category = Category::find($id);
            if($category != null)
                $category->deleteAll();
        }
        catch(Exception $ex)
        {
            $result->result = false;
        }
        return $result;

    }

}

?>