<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;

class CustomResponse
{
	public $form =[];
	public $data=[];
	public $messages=null;
	public $message=null;
	public $result = false;
	public $model = false;
	public function __construct()
	{
		$this->messages = new Collection();
	}


	public function ToJson()
	{
		return json_encode($this);
	}

	public function pushErrors($errors)
	{
		foreach($errors->toArray() as $key =>  $value)
		{
			$this->form[$key] = new FormProperty($value[0],"danger");
			$this->messages->add(new FormMessage($value[0], "danger"));
		}

	}
	public  function  pushMessage($msg){
		$this->messages->add($msg);
	}
}
