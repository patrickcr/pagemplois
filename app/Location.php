<?php

namespace App;

use App\Dataset;
use App\Http\Requests\Request;
use App\Location;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];
    protected $childrens = [];

    public function Advertisement()
    {
        return $this->belongsToMany('App\Advertisement', 'advertisement_locations');
    }


    public function childrens()
    {
        $this->childrens = new Collection();
        $this->getChildren($this->id);
        return $this->childrens;
    }

    public function getChildren($id)
    {
        $children_data = $this->where('parent', '=', $id)->get();
        foreach ($children_data as $children) {
            $this->childrens->add($children);
            $this->getChildren($children->id);
        }
    }

    public function deleteAll()
    {
        $childrens = $this->childrens();
        foreach ($childrens as $children)
            $children->deleteAll();
        $this->delete();
    }

    public function scopeEdit($query, $id)
    {
        if($id > 0)
            $location = Location::find($id); 
        else
        {
            $location = new Location();
            $location->id = -1;
            $location->name = "";
            $location->reference = "";
            $location->active = 1;
            $location->parent = null;
            $location->order = 0;
        }
        
        return $location;
    }

    public function scopeParent($query, $id)
    {
        
        return Location::where('id', '!=', $id)->get();
    }


    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        foreach ($filters as $key => $value) {


            if($value != '')
            { 
                $query = $query->where($key,'LIKE', '%'.$value.'%');   
            }
        }

        $sorts = $request->get("sorts");
        foreach ($sorts as $key => $value) {


            if($value != '')
            {

                $query = $query->OrderBy($key,$value);   

            }
        }


        return new Dataset($query, $skip, $take);

    }

    public function scopeActive($query)
    {
        return $query->where('active', '=', 1)->orderBy('parent', 'asc')->orderBy('order', 'asc')->get();
    }

    public function UI($item)
    {
        $childrens = Location::where('parent', '=', $item->id)->where('active', '=', 1)->get();
        if ($childrens->count() > 0) {
            $li = '<li class="check-item"> <i data-id="' . $item->id . '" class="fa fa-square-o check-control"></i><i class="fa fa-chevron-right" ';
            $li .= 'data-toggle="collapse" data-target="#c-' . $item->id . '"></i>';
            $li .= $item->name . '<ul class="collapse" id="c-' . $item->id . '">';
            foreach ($childrens as $child)
                $li .= $child->UI($child);
            $li .= '</ul></li>';
            return $li;
        } else
        return '<li class="check-item"><i data-id="' . $item->id . '" class="fa fa-square-o check-control"></i><div>' . $item->name . '</div></li>';
    }


}
