<?php

namespace App\Users;

use App\FormResult;
use App\Location;
use App\User;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserBO
{
	use ValidateRequestsForApp;


	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function save()
	{

		return $this->validateRequest()->saveDB();
		
	}


    public function messages(){

        return [
        'name.required' => "Le nom ce um champ necessaire",
        'email.required'  => 'La email ce un champ necessaire',
        'email.exists' => "Cette email exist dejá",
        'password.required' => "Password ce um champ necessaire",
        ];
    }

    public function validateRequest()
    {

        if($this->request->id == -1)
        {
            $this->validate($this->request, [
                'name' => 'required',
                'email' => 'required|unique:users',
                'password' => 'required'
                ], $this->messages());
        }
        else
        {

          $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required',
            ], $this->messages());

         }


      return $this;

  }





  public function saveDB()
  {


    $result = new FormResult();

    try
    {

        if($this->request->id > 0)
            $user = User::find($this->request->id);
        else
            $user = new User();

        $user->fill($this->request->all());

        $user->save();

        if(!empty($this->request->password))
        {
            $user->password = Hash::make($this->request->password);
            $user->save();

        }
    }
    catch(Exception $ex)
    {

        $result->result = false;
    }

    return $result->toJson();

}

public function delete($id)
{
    $result = new FormResult();
    try{
        $location = User::find($id);
        if($location != null)
            $location->delete();
    }
    catch(Exception $ex)
    {
        $result->result = false;
    }
    return $result;

}

}

?>