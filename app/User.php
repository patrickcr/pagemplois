<?php

namespace App;


use App\User;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    protected $guarded = ['id','password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public function Advertisement(){
        return $this->hasMany('App\Advertisement');
    }
    public function hasRole($roles)
    {
        foreach ($roles as $role) {
            if ($this->type == $this->ParseRole($role))
                return true;
        }
        return false;
    }

    public function ParseRole($role)
    {
        switch ($role) {

            case 'admin':
            return 1;
            break;

            case  'customer':
            return 2;
            break;
        }
    }

    public function isAdmin()
    {
        if ($this->type == 1)
            return true;
        return false;
    }


    public function isCustomer()
    {
        if ($this->type == 2)
            return true;
        return false;
    }


    public function accessMediasAll()
    {
        return true;

    }

    public  function scopebyEmail($query, $email)
    {
        return $query->where('email', $email)->get()->first();
    }


    public function scopeEdit($query, $id)
    {
        if($id > 0)
            $user = User::find($id); 
        else
        {
            $user = new User();
            $user->id = -1;
            $user->name = "";
            $user->email = "";
            $user->password = "";
            $user->type = 2;
            $user->active = 0;
        }
        
        return $user;
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        foreach ($filters as $key => $value) {


            if($value != '')
            { 
                $query = $query->where($key,'LIKE', '%'.$value.'%');   
            }
        }

        $sorts = $request->get("sorts");
        foreach ($sorts as $key => $value) {


            if($value != '')
            {

                $query = $query->OrderBy($key,$value);   

            }
        }


        return new Dataset($query, $skip, $take);

    }

}
