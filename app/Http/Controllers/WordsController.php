<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Language;
use App\Translation;
use App\Word;
use App\Words\WordBO;
use Illuminate\Http\Request;

class WordsController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
      {
        return  Word::Page($request)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('words.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $word = Word::Edit($id);
        $languages = Language::all();


        foreach ($languages as $language) {

            if($word->Translations->where('language_id', $language->id)->count() == 0)
            {
                $translation = new Translation();
                $translation->language_id = $language->id;
                $translation->language = $language;
                $translation->value = "";
                $word->Translations->add($translation);
            }

        }
        return view('words.edit')->with('word', $word);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, WordBO $words)
    {

        return $words->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,  WordBO $words)
    {
        return $words->delete($id)->toJson();
    }
}
