<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Http\Requests;
use App\Language;
use App\Languages\LanguageBO;
use App\Size;
use App\Sizes\SizeBO;
use Illuminate\Http\Request;

class SizesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Size::Page($request)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function templatesShow($id)
    {
        $size = Size::find($id);
        $languages = Language::Active();

        return view('sizes.templates')->with("size", $size)->with("languages", $languages);
    }

    public function templates($id)
    {
        return Advertisement::Templates($id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('sizes.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size = Size::Edit($id);

        return view('sizes.edit')->with('size', $size);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, SizeBO $sizes)
    {

        return $sizes->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, SizeBO $sizes)
    {
        return $sizes->delete($id)->toJson();
    }
}
