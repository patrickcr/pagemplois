<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Category;
use App\Contract_Type;
use App\Http\Requests;
use App\Location;
use App\Size;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{

    public function index()
    {

        return view('index');

    }

    public function admin()
    {

        return view('admin.index');
    }
}
