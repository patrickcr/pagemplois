<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Category;
use App\Contract_Type;
use App\Location;
use App\Size;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('customers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sizes = Size::Active();
        if ($id == "new")
            $advertisement = Advertisement::New();
        else
            $advertisement = Advertisement::with('size')->find($id);


        if (!empty($advertisement) && $advertisement->count() > 0) {

            if ($advertisement->id > 0 && $advertisement->User->id != Auth::User()->id)
                return redirect()->route('dashboard');

            $categories = Category::Active();
            $locations = Location::Active();
            $contract_types = Contract_Type::Active();


            $wizard = view('wizard.index')->with('advertisement', $advertisement)->with('sizes', $sizes)
                ->with('categories', $categories)->with('locations', $locations)->with('contract_types', $contract_types)->with("languages", [])->with("workflow", "customers");

            return view('customers.wizard')->with('wizard', $wizard);
        } else
            return redirect()->route('dashboard');


    }

    public function customer()
    {
        return Advertisement::Customer();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
