<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Advertisements\AdvertisementBO;
use App\Category;
use App\Contract_Type;
use App\Language;
use App\Location;
use App\Size;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdvertisementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "index";
    }

    public function search(Request $request)
    {
        return Advertisement::PubBySize(1)->ToJson();

    }

    public function template($id)
    {


        $sizes = Size::All();


        if ($id > 0)
            $advertisement = Advertisement::with('size')->find($id);
        else {


            $advertisement = Advertisement::New();
            $advertisement->size_id = -$id;
            $advertisement->size = Size::find($advertisement->size_id);
            //set type to pub
            $advertisement->type = 2;
            $advertisement->status = 0;
        }

        //set the workflow

        $workflow = 'sizes/' . $advertisement->size_id . '/templates';
        if (!empty($advertisement) && $advertisement->count() > 0) {


            $advertisement->language_id = Language::Default()->id;

            $categories = Category::Active();
            $locations = Location::Active();
            $contract_types = Contract_Type::Active();

            $wizard = view('wizard.index')->with('advertisement', $advertisement)->with('sizes', $sizes)
                ->with('categories', $categories)->with('locations', $locations)->with('contract_types', $contract_types)->with('languages', Language::Active())->with("workflow", $workflow);

            return view('sizes.template')->with('wizard', $wizard);
        } else
            return redirect()->route('admin');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, AdvertisementBO $advertisements)
    {
        return $advertisements->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, AdvertisementBO $advertisements)
    {
        return $advertisements->delete($id)->toJson();
    }
}
