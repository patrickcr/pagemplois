<?php

namespace App\Http\Controllers;

use App\Auth\Authentication;
use App\Http\Requests;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the auth form for login
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function loginView()
    {
        return view('auth.index')->with('step', 'login');
    }


    /**
     * Show the auth form for register
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function registerView()
    {
          return view('auth.index')->with('step', 'register');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Authentication $auth)
    {
        return $auth->login();
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout(Authentication $auth)
    {
        return $auth->logout();
    }
}
