<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable =[];

    public function Language()
    {
        return $this->belongsTo('App\Language');
    }

    public function Word()
    {
        return $this->belongsTo('App\Word');
    }

    public function scopeTranslationByLanguage($query, $language_id)
    {
        return $query->where('language_id','=', $language_id)->get();
    }

}
