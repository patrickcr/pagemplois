<?php

namespace App\Languages;

use App\FormResult;
use App\Language;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class LanguageBO
{
	use ValidateRequestsForApp;


	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function save()
	{

		return $this->validateRequest()->saveDB();
		
	}


    public function messages(){

        return [
        'name.required' => "Le nom de la language es necessaire",
        'name.exists' => "Cette language exist dejá",
        'code.required'  => 'La code de la langue necessaire',
        'code.exists' => "Ce code exist dejá",
        ];
    }

    public function validateRequest()
    {
      
      if($this->request->id == -1)
      {
        $this->validate($this->request, [
            'name' => 'required|unique:languages',
            'code' => 'required|unique:languages'
            ], $this->messages());

        
        }//$this->throwValidationException($request, $validator);


        return $this;

    }




    public function resetDefault(){

        Language::where('default', 1)->update(['default' => 0]);
    }


    public function saveDB()
    {
      

        $result = new FormResult();

        try
        {

            if($this->request->id > 0)
                $language = Language::find($this->request->id);
            else
                $language = new Language();

            $language->fill($this->request->all());


            $language->save();

            if($language->default == 1)
                Language::where('id', '!=', $language->id)->update(['default' => 0]);


       
        }
        catch(Exception $ex)
        {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try{
            $language = Language::find($id);
            $language->delete();
        }
        catch(Exception $ex)
        {
            $result->result = false;
        }
        return $result;

    }

}

?>