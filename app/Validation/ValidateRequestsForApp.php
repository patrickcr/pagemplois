<?php

namespace App\Validation;

use App\Field;
use App\FormResult;
use Illuminate\Foundation\Validation\ValidatesRequests;


trait ValidateRequestsForApp
{
	use ValidatesRequests;
	

	public function formatValidationErrors($validator)
	{	
		$result  = new FormResult(false);

		foreach ($validator->errors()->messages() as $key => $value) {
			
			$field = new Field($key, false, 'has-error',$value[0]);
			$result->addField($field);
		}
		return $result->toArray();
	}
	
}

?>