<?php

namespace App\Locations;

use App\FormResult;
use App\Location;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class LocationBO
{
	use ValidateRequestsForApp;


	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function save()
	{

		return $this->validateRequest()->saveDB();
		
	}


    public function messages(){

        return [
        'name.required' => "Le nom de lu plave es necessaire",
        'name.exists' => "Cette place exist dejá",
        'reference.required'  => 'La reference ce un champ necessaire',
        'reference.exists' => "Cette reference exist dejá",
        ];
    }

    public function validateRequest()
    {
      
      if($this->request->id == -1)
      {
        $this->validate($this->request, [
            'name' => 'required|unique:categories',
            'reference' => 'required|unique:categories'
            ], $this->messages());

        
        }//$this->throwValidationException($request, $validator);


        return $this;

    }





    public function saveDB()
    {
      

        $result = new FormResult();

        try
        {

            if($this->request->id > 0)
                $location = Location::find($this->request->id);
            else
                $location = new Location();

            $location->fill($this->request->all());

            $location->save();

        }
        catch(Exception $ex)
        {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try{
            $location = Location::find($id);
            if($location != null)
                $location->deleteAll();
        }
        catch(Exception $ex)
        {
            $result->result = false;
        }
        return $result;

    }

}

?>