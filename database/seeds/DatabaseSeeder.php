<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();
        $this->call(SizeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(ContractTypeSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(AdvertisementSeeder::class);

        //Create Ads for Customer

        Model::reguard();

    }
}
