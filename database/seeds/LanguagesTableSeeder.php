<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language_en = new \App\Language();
        $language_en->name = 'English';
        $language_en->code = 'EN';
        $language_en->default = 1;
        $language_en->active = 1;
        $language_en->save();

        $language_fr = new \App\Language();
        $language_fr->name = 'French';
        $language_fr->code = 'FR';
        $language_en->active = 1;
        $language_fr->save();

        //Add Words
        //Categories
        $word = new \App\Word();
        $word->reference = 'it-telecomunciations';
        $word->value = 'IT & Telecomunications';
        $word->save();

        //Translations English
        $translation = new \App\Translation();
        $translation->language_id = $language_en->id;
        $translation->value = 'IT & Telecomunications';
        $word->Translations()->save($translation);

        //Translations French
        $translation = new \App\Translation();
        $translation->language_id = $language_fr->id;
        $translation->word_id = $word->id;
        $translation->value = 'Informatique & Telecomunciations';
        $word->Translations()->save($translation);


        //NEW WORD FOR building-construction-civil-engineering
        $word = new \App\Word();
        $word->reference = 'building-construction-civil-engineering';
        $word->value = 'Building / Construction / Civil Engineering';
        $word->save();

        //Translations English
        $translation = new \App\Translation();
        $translation->language_id = $language_en->id;
        $translation->value = 'Building / Construction / Civil Engineering';
        $word->Translations()->save($translation);

        //Translations French
        $translation = new \App\Translation();
        $translation->language_id = $language_fr->id;
        $translation->word_id = $word->id;
        $translation->value = 'Bâtiment / Construction / Travaux publics';
        $word->Translations()->save($translation);



        /*LOCATIONS*/

        $word = new \App\Word();
        $word->reference = 'geneva';
        $word->value = 'Geneva';
        $word->save();

        $translation = new \App\Translation();
        $translation->language_id = $language_en->id;
        $translation->value = 'Geneva';
        $word->Translations()->save($translation);

        $translation = new \App\Translation();
        $translation->language_id = $language_fr->id;
        $translation->value = 'Geneva';
        $word->Translations()->save($translation);



    }
}
