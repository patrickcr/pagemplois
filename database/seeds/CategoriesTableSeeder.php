<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new \App\Category();
        $category->id = 1;
        $category->name = "Animaux & accessoires";
        $category->reference = 'it-telecomunciations';
        $category->active = 1;
        $category->save();

        $child_category = new \App\Category();
        $child_category->id = 2;
        $child_category->name = "Developer";
        $child_category->active = 1;
        $child_category->reference = 'developer';
        $child_category->parent = $category->id;
        $child_category->save();


        //Category Building / Construction / Civil Engineering
        $category = new \App\Category();
        $category->id = 3;
        $category->name = "Building / Construction / Civil Engineering";
        $category->active = 1;
        $category->reference = 'building-construction-civil-engineering';
        $category->save();

    }
}
