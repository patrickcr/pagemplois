<?php

use Illuminate\Database\Seeder;

class ContractTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contract_type = new \App\Contract_Type();
        $contract_type->name = 'Permanent';
        $contract_type->reference = 'permanent';
        $contract_type->active = 1;
        $contract_type->order = 1;
        $contract_type->save();

        $contract_type = new \App\Contract_Type();
        $contract_type->name = 'Contract';
        $contract_type->reference = 'contract';
        $contract_type->active = 1;
        $contract_type->order = 2;
        $contract_type->save();

        $contract_type = new \App\Contract_Type();
        $contract_type->name = 'Freelance';
        $contract_type->reference = 'freelance';
        $contract_type->active = 1;
        $contract_type->order = 3;
        $contract_type->save();

        $contract_type = new \App\Contract_Type();
        $contract_type->name = 'Part-Time';
        $contract_type->reference = 'part-time';
        $contract_type->active = 1;
        $contract_type->order = 4;
        $contract_type->save();

    }
}
