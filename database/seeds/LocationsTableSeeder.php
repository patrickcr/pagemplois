<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $location = new \App\Location();
        $location->name = 'Geneva';
        $location->active = true;
        $location->reference = 'geneva';
        $location->save();

    }
}
