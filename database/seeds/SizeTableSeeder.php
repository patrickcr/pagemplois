<?php

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++) {
            for ($j = 1; $j <= 3; $j++) {
                $size = new \App\Size();
                $size->name = $i . 'x' . $j;
                $size->class = 'a' . $i . 'x' . $j;
                $size->active = 0;
                if ($size->name == '1x1'  || $size->name == '1x2' || $size->name == '1x3'   ||
                    $size->name == '2x1'  || $size->name == '2x2' || $size->name == '2x3')
                    $size->active = 1;
                $size->save();
            }
        }
    }
}
