<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Developer
        $user = new \App\User();
        $user->name='Patrick Cabral Reis';
        $user->email = "patrickcr@gmail.com";
        $user->password = Hash::make('123asd');
        $user->type = 1;
        $user->save();

        //Admin
        $user = new \App\User();
        $user->name= 'Advertiser';
        $user->email = "advertiser@pagemplois.com";
        $user->password = Hash::make('123asd');
        $user->type = 2;
        $user->save();

        //Client
        $user = new \App\User();
        $user->name= 'Candidate';
        $user->email = "candidate@pagemplois.com";
        $user->password = Hash::make('123asd');
        $user->type = 3;
        $user->save();
    }
}
