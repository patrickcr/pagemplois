<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {

            echo "Create Advertisements table \n";

            $table->increments('id');
            //structure
            $table->string('theme')->nullable();
            
            //structure
            $table->string('structure')->nullable();

            //Background Image
            $table->string('backgroundActive')->default(0);
            $table->string('backgroundUrl')->nullable();


            //logo
            $table->string('logoActive')->default(0);
            $table->string('logoUrl')->nullable();

            //header
            $table->string('headerActive')->default(0);
            $table->string('headerStyle')->nullable();
            $table->text('headerText')->nullable();
            
            //content
            $table->string('contentActive')->default(0);
            $table->string('contentStyle')->nullable();
            $table->text('contentText')->nullable();
            
            //footer
            $table->string('footerActive')->default(0);
            $table->string('footerStyle')->nullable();
            $table->text('footerText')->nullable();

            // 1 for user advertisment 2 for pub advertisment
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('language_id')->nullable()->unsigned()->length(10);
            $table->integer('size_id')->unsigned()->length(10);
            $table->integer('user_id')->unsigned()->length(10);
            $table->timestamp('valid_to');
            
            //advertiment type Offer or Application
            $table->tinyInteger('offer')->nullable();
            $table->tinyInteger('application')->nullable();

            $table->tinyInteger('mail_offers')->nullable();
            $table->tinyInteger('mail_applications')->nullable();

            $table->timestamps();


            //FOREIGN KEYS
            echo "Adding Size Foreign Key \n";
            $table->foreign('size_id', 'ref_size')->references('id')->on('sizes');

            echo "Adding User Foreign Key \n";
            $table->foreign('user_id', 'ref_user')->references('id')->on('users');

        });

        echo "***********************\n";

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advertisements');

    }
}
