<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContractTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Contract Types table\n";

        Schema::create('contract_types', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('reference');
            $table->tinyInteger('order');
            $table->tinyInteger('active');
            $table->timestamps();

        });

        echo "***********************\n";
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
