<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Settings table\n";

        Schema::create('settings', function (Blueprint $table) {
            //general info
            $table->increments('id');
            $table->string('name');
            $table->string('slogan')->nullable();
            $table->string('email');
            $table->string('support_email');
            $table->string('no_reply_email');
            $table->string('country');
            $table->string('address');
            $table->string('number');
            $table->string('postcode');
            //definitions
            $table->string('language');
            $table->string('currency');
            $table->string('tva');

            $table->timestamps();


        });

        echo "***********************\n";

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
