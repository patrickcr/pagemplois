<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Locations table\n";

        Schema::create('locations', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('active');
            $table->string('reference');
            $table->tinyInteger('order')->nullable();
            $table->integer('parent')->nullable()->length(10);
            $table->timestamps();

        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
