<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsPayementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Payements table\n";

        Schema::create('advertisements_payements', function (Blueprint $table) {

            echo "Create Advertisements Payements table \n";

            $table->increments('id');
            //Number of transaction
            $table->string('transaction');
            //Type of Payement
            //1: paypal
            //2 : poste
            $table->tinyInteger('payement_type');
            $table->tinyInteger('status');
            $table->decimal('amount');
            $table->integer('advertisement_id')->unsigned()->length(10);
            $table->timestamps();

            //FOREIGN KEYS
            echo "Adding Size Foreign Key \n";
            $table->foreign('advertisement_id', 'ref_advertisement')->references('id')->on('advertisements');

        });


        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
