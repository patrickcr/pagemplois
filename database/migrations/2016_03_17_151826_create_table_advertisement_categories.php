<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvertisementCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Advertisement Categories table\n";

        Schema::create('advertisement_categories', function (Blueprint $table) {

            $table->integer('advertisement_id')->unsigned()->length(10);
            $table->integer('category_id')->unsigned()->length(10);
            $table->timestamps();

            echo "adding foreign key Advertisement Category \n";

            $table->foreign('advertisement_id','ref_ac_advertisement')->references('id')->on('advertisements')->onDelete('cascade');
            $table->foreign('category_id','ref_category')->references('id')->on('categories')->onDelete('cascade');
            $table->primary(array('advertisement_id', 'category_id'));
        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
