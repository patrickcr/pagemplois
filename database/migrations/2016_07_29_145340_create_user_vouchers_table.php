<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_vouchers', function (Blueprint $table) {

            echo "Create Vouchers table \n";

            $table->increments('id');
            //structure
            $table->integer('size_id')->unsigned()->length(10);
            $table->integer('user_id')->unsigned()->length(10);
            $table->timestamp('valid_to');
            $table->timestamps();


            //FOREIGN KEYS
            echo "Adding Size Foreign Key \n";
            $table->foreign('size_id', 'ref_size_voucher')->references('id')->on('sizes');

            echo "Adding User Foreign Key \n";
            $table->foreign('user_id', 'ref_user_voucher')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_vouchers');
    }
}
