<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvertisementContractTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Advertisement ContractType table\n";

        Schema::create('adv_contract_types', function (Blueprint $table) {

            $table->integer('advertisement_id')->unsigned()->length(10);
            $table->integer('contract_type_id')->unsigned()->length(10);
            $table->timestamps();

            echo "adding foreign key Advertisement Contracts \n";

            $table->foreign('advertisement_id','ref_act_ad')->references('id')->on('advertisements')->onDelete('cascade');
            $table->foreign('contract_type_id','ref_contract_type')->references('id')->on('contract_types')->onDelete('cascade');
            $table->primary(array('advertisement_id', 'contract_type_id'));


        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
