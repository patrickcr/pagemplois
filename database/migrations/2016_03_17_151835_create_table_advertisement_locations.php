<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvertisementLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Advertisement locations table\n";

        Schema::create('advertisement_locations', function (Blueprint $table) {

            $table->integer('advertisement_id')->unsigned()->length(10);
            $table->integer('location_id')->unsigned()->length(10);
            $table->timestamps();

            echo "adding foreign key Advertisement Locations \n";

            $table->foreign('advertisement_id','ref_al_advertisement')->references('id')->on('advertisements')->onDelete('cascade');
            $table->foreign('location_id','ref_location')->references('id')->on('locations')->onDelete('cascade');
            $table->primary(array('advertisement_id', 'location_id'));

        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
